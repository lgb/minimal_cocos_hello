@echo off
REM
REM Clean script
REM

REM VisualStudio solution stuff
DEL /A /F /Q *.sdf
DEL /A /F /Q *.suo

REM compiller output directories
RMDIR /S /Q bin
RMDIR /S /Q Debug
RMDIR /S /Q ipch
RMDIR /S /Q Release
