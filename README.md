# Minimal Cocos Hello

This is my try to create minimalist windows application like Cocos2Dx provides for iOS.

I uses Visual C++ 2012 Express, just copied cocos2d and HelloCpp directories from Cocos2D-x 2.0.4 and create new empty solution and project. Then I add all files from cocos and hellocpp projects to new project and change some options.

On this build, Cococ2Dx don't used as separate shared library - it's compiled and statically linked together with sample project. But Cocos2Dx distribution provides some libraries as binary dll and I just reuse it. Ideally, we need find sources for this libraries and integrate it to project too.

PS: It's only Win32 project, not other platforms.
